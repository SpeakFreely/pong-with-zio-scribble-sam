# Pong With ZIO Scribble SAM

Proof of concept and demo program using ZIO, scribble-scala and SAM (State Action Model)
Pong is a two person real time game, using two browsers on different computers.
Written in pure scala and compiled using scala-js